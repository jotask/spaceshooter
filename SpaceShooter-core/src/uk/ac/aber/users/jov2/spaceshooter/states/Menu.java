package uk.ac.aber.users.jov2.spaceshooter.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

import uk.ac.aber.users.jov2.spaceshooter.Application;
import uk.ac.aber.users.jov2.spaceshooter.util.Background;
import uk.ac.aber.users.jov2.spaceshooter.util.Banner;
import uk.ac.aber.users.jov2.spaceshooter.util.GameStateManager;
import uk.ac.aber.users.jov2.spaceshooter.util.GameStateManager.StateHolder;
import uk.ac.aber.users.jov2.spaceshooter.util.effects.Stars;

public class Menu implements State {

	private Stage stage;
	private Table table;
	private Skin skin;
	
	private Banner logo;
	private Background bg;
	private Stars particle;
	
	@Override
	public void init() {
		skin = new Skin(Gdx.files.internal("skin/uiskin.json"));
		stage = new Stage();
		table = new Table();
		table.setFillParent(true);
		stage.addActor(table);
		
		bg = new Background("bg.png", stage.getCamera());
		particle = new Stars();
		
		logo = new Banner();
		table.add(logo).row();
		
		addButtons();
		
		Gdx.input.setInputProcessor(stage);
		Gdx.gl.glClearColor(.135f, .206f, .235f, 1);
	}
	
	private void addButtons(){
		// Start Game
		final TextButton startGame = new TextButton("StartGame", skin);
		startGame.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				Application.gsm.changeState(StateHolder.GAME);
			}
		});
		table.add(startGame).fill();
		table.row();
		
		// Options
		final TextButton options = new TextButton("Options", skin);
		options.addListener(new ChangeListener(){
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				Application.gsm.changeState(StateHolder.OPTIONS);
			}
		});
		table.add(options).fill().padTop(10);
		table.row();
		
		// Exit
		final TextButton exit = new TextButton("Exit", skin);
		exit.addListener(new ChangeListener(){
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				Gdx.app.exit();
			}
		});
		table.add(exit).fill().padTop(10);
		table.row();
		
	}

	@Override
	public void update(GameStateManager gsm, float delta) {
		stage.act(delta);
		particle.update(delta);
	}

	@Override
	public void render(SpriteBatch sb) {
		sb.setProjectionMatrix(stage.getCamera().combined);
		sb.begin();
		bg.render(sb);
		particle.render(sb);
		sb.end();
		
		table.setDebug(Application.debug);
		stage.draw();
		
		particle.reset();
	}

	@Override
	public void debug(ShapeRenderer sr) { }

	@Override
	public void dispose() {
		stage.dispose();
		skin.dispose();
		bg.dispose();
		particle.dispose();
		logo.dispose();
	}

}
