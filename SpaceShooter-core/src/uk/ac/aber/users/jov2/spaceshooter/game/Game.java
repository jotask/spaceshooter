package uk.ac.aber.users.jov2.spaceshooter.game;

import java.util.ArrayList;
import java.util.Iterator;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import uk.ac.aber.users.jov2.spaceshooter.Application;
import uk.ac.aber.users.jov2.spaceshooter.game.entity.Enemy;
import uk.ac.aber.users.jov2.spaceshooter.game.entity.Entity;
import uk.ac.aber.users.jov2.spaceshooter.game.entity.Player;
import uk.ac.aber.users.jov2.spaceshooter.game.entity.bullet.Bullet;
import uk.ac.aber.users.jov2.spaceshooter.game.ui.hud.Hud;
import uk.ac.aber.users.jov2.spaceshooter.game.world.Level;
import uk.ac.aber.users.jov2.spaceshooter.states.State;
import uk.ac.aber.users.jov2.spaceshooter.util.Camera;
import uk.ac.aber.users.jov2.spaceshooter.util.GameStateManager;

public class Game implements State {

	private Camera cam;
	public static Level level;

	private Player player;
	private Hud hud;

	public static ArrayList<Enemy> enemies;
	public static ArrayList<Bullet> bullets;

	@Override
	public void init() {
		cam = new Camera();
		level = new Level(cam);
		player = new Player();
		hud = new Hud(player);

		enemies = new ArrayList<Enemy>();
		enemies.add(new Enemy());
		bullets = new ArrayList<Bullet>();

	}

	@Override
	public void update(GameStateManager gsm, float delta) {
		level.update(delta, cam);
		updatePlayer(delta);
		updateEnemies(delta);
		updateBullets(delta);
		hud.update(delta);
		cam.update(player.getPosition());
	}
	
	private void updatePlayer(float delta){
		player.update(delta);
	}

	private void updateEnemies(float delta) {
		ArrayList<Entity> delete = new ArrayList<Entity>();
		Iterator<Enemy> e = enemies.iterator();
		while (e.hasNext()) {
			Enemy enemy = e.next();
			enemy.update(delta);
			if(enemy.isDead())
				delete.add(enemy);
			}
		deleteEntities(delete);
	}
	
	private void updateBullets(float delta){
		ArrayList<Entity> delete = new ArrayList<Entity>();
		Iterator<Bullet> b = bullets.iterator();
		while (b.hasNext()){
			Bullet bullet = b.next();
			bullet.update(delta);
			if(bullet.death())
				delete.add(bullet);
		}
		deleteEntities(delete);
	}
		
	private void deleteEntities(ArrayList<Entity> ent){
		Iterator<Entity> entity = ent.iterator();
		while(entity.hasNext()){
			Entity e = entity.next();
			level.getWorld().destroyBody(e.getBody());
			if(e instanceof Bullet){
				bullets.remove(e);
			}else if(e instanceof Enemy){
				enemies.remove(e);
			}
		}
	}

	@Override
	public void render(SpriteBatch sb) {
		sb.setProjectionMatrix(cam.combined);
		sb.begin();
		level.render(sb);
		renderEnemies(sb);
		renderBullets(sb);
		player.render(sb);
		sb.end();
		
		hud.render();

		if (Application.debug)
			level.debug(cam.combined);
	}
	
	private void renderEnemies(SpriteBatch sb){
		Iterator<Enemy> ent = enemies.iterator();
		while(ent.hasNext())
			ent.next().render(sb);
	}
	
	private void renderBullets(SpriteBatch sb){
		Iterator<Bullet> bullet = bullets.iterator();
		while(bullet.hasNext())
			bullet.next().render(sb);
	}

	@Override
	public void debug(ShapeRenderer sr) {
	}

	@Override
	public void dispose() {
		level.dispose();
		player.dispose();
	}

}
