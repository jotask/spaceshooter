package uk.ac.aber.users.jov2.spaceshooter.game.entity;

public abstract class LiveEntity extends Entity{
	
	protected float health;
	protected float currentHealth;
	
	public LiveEntity (float health){
		this.health = health;
		this.currentHealth = health;
	}
	
	public void addHealth(float amount){
		currentHealth += amount;
	}
	
	public void takeDamage(float amount){
		currentHealth -= amount;
	}
	
	public boolean isDead(){
		if(currentHealth <= 0)
			return true;
		return false;
	}

}
