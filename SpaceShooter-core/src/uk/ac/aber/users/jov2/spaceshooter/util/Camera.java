package uk.ac.aber.users.jov2.spaceshooter.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;

public class Camera extends OrthographicCamera{

	private int SCALE;
	
	private boolean followOnlyX;
	
	public Camera(){
		this(true, 4);
	}
	
	public Camera(boolean followOnlyX, int scale) {
		super();
		this.followOnlyX = followOnlyX;
		this.SCALE = scale;
		this.setToOrtho(false, Gdx.graphics.getWidth() / SCALE, Gdx.graphics.getHeight() / SCALE);
		this.position.set(0, 0, 0);
		this.update();
	}
	
	public void update(Vector2 player){
		if(followOnlyX){
			this.position.set(player.x, this.position.y, 10);
		}else{
			this.position.set(player.x, player.y, 10);
		}
		this.update();
	}
	
	public Vector2 get2D(){
		return new Vector2(this.position.x, this.position.y);
	}
	
}
