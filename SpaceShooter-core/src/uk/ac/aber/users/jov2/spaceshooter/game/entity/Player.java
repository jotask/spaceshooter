package uk.ac.aber.users.jov2.spaceshooter.game.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;

import uk.ac.aber.users.jov2.spaceshooter.game.GunController;
import uk.ac.aber.users.jov2.spaceshooter.util.Constants;
import uk.ac.aber.users.jov2.spaceshooter.util.Sprite;

public class Player extends LiveEntity {
	
	protected static final float MAX_VELOCITY = 200;
	protected static final float VELOCITY = 7f;
	
	public static final float SIZE = 7;
	public static final float HEALTH = 100;
	
	private Sprite sprite;
	private GunController gun;
	
	public Player() {
		super(HEALTH);

		// First we create a body definition
		BodyDef bodyDef = new BodyDef();
		// We set our body to dynamic, for something like ground which doesn't move we would set it to StaticBody
		bodyDef.type = BodyType.DynamicBody;
		// Set our body's starting position in the world
		bodyDef.position.set(0, 0);

		// Create a circle shape and set its radius to 6
		CircleShape shape = new CircleShape();
		shape.setRadius(SIZE);

		// Create a fixture definition to apply our shape to
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = shape;
		fixtureDef.density = 0.01f; 
		fixtureDef.friction = 100f;
		fixtureDef.filter.categoryBits = Constants.PLAYER_BITS;
		fixtureDef.filter.maskBits = Constants.PLAYER_MASK;
		
		this.createBody(bodyDef, shape, fixtureDef);
		body.setFixedRotation(true);
		body.setUserData(this);
		sprite = new Sprite(Gdx.files.internal("playerShip2_red.png"), SIZE, SIZE);
		gun = new GunController(this);
	}

	@Override
	public void update(float delta) {
		Vector2 vel = this.body.getLinearVelocity();
		Vector2 pos = body.getPosition();
		
		// apply left impulse, but only if max velocity is not reached yet
		if (Gdx.input.isKeyPressed(Constants.LEFT) && vel.x > - MAX_VELOCITY) {          
		     body.applyLinearImpulse(-VELOCITY, 0, pos.x, pos.y, true);
		}

		// apply right impulse, but only if max velocity is not reached yet
		if (Gdx.input.isKeyPressed(Constants.RIGTH) && vel.x < MAX_VELOCITY) {
		     body.applyLinearImpulse(VELOCITY, 0, pos.x, pos.y, true);
		}

		// apply up impulse, but only if max velocity is not reached yet
		if (Gdx.input.isKeyPressed(Constants.UP) && vel.y < MAX_VELOCITY) {
		     body.applyLinearImpulse(0, VELOCITY, pos.x, pos.y, true);
		}

		// apply down impulse, but only if max velocity is not reached yet
		if (Gdx.input.isKeyPressed(Constants.DOWN) && vel.y < MAX_VELOCITY) {
		     body.applyLinearImpulse(0, -VELOCITY, pos.x, pos.y, true);
		}
		
		if(Gdx.input.isKeyPressed(Constants.A)){
			gun.shoot();
		}
		
		if(Gdx.input.isKeyJustPressed(Keys.R)){
			body.setLinearVelocity(0,0);
			body.setAngularVelocity(0);
		}
		
	}

	@Override
	public void render(SpriteBatch sb) {
		sprite.render(sb, body.getPosition());
	}
	
	@Override
	public void debug(ShapeRenderer sr) { }
	
	public void dispose(){
		sprite.dispose();
	}
	
	public float getLife(){
		return this.currentHealth;
	}

}
