package uk.ac.aber.users.jov2.spaceshooter.game.world;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;

import uk.ac.aber.users.jov2.spaceshooter.util.Camera;
import uk.ac.aber.users.jov2.spaceshooter.util.Constants;

public class Bound {
	
	private Body body;
	
	private final float OFFSET = -1;
	
	public Bound(World world, Camera cam) {
		BodyDef bd = new BodyDef();
		bd.type = BodyType.StaticBody;
		bd.position.set(cam.position.x, cam.position.y);
		body = world.createBody(bd);
		body.setUserData(this);
		createFixture(cam);
	}
	
	private void createFixture(Camera cam){
		Vector2 p = cam.get2D();
		float width = cam.viewportWidth / 2;
		float height = cam.viewportHeight / 2;
		Vector2 v1, v2;
		// Top ///////////////////////////////////////////////////////////
		v1 = new Vector2(p.x - width - OFFSET, p.y + height - (-OFFSET));
		v2 = new Vector2(p.x + width + OFFSET, v1.y);
		body.createFixture(createFixture(v1, v2));
		
		// Bottom ///////////////////////////////////////////////////////////
		v1 = new Vector2(p.x - width - OFFSET, p.y - height + (-OFFSET));
		v2 = new Vector2(p.x + width + OFFSET, v1.y);
		body.createFixture(createFixture(v1, v2));
		
		// Left ///////////////////////////////////////////////////////////
		v1 = new Vector2(p.x - width + (-OFFSET), p.y + height + OFFSET);
		v2 = new Vector2(v1.x, p.y - height - OFFSET);
		body.createFixture(createFixture(v1, v2));
		
		// Right ///////////////////////////////////////////////////////////
		v1 = new Vector2(p.x + width - (-OFFSET), p.y - height - OFFSET);
		v2 = new Vector2(v1.x, p.y + height + OFFSET);
		body.createFixture(createFixture(v1, v2));
		
	}
	
	private FixtureDef createFixture(Vector2 v1, Vector2 v2){
		EdgeShape shape = new EdgeShape();
		shape.set(v1, v2);
		
		FixtureDef fd = new FixtureDef();
		fd.shape = shape;
		fd.filter.categoryBits = Constants.BOUNDS_BITS;
		fd.filter.maskBits = Constants.BOUNDS_MASK;
		
		return fd;
	}
	
	public void update(Vector2 position){
		body.setTransform(position, 0);
	}

}
