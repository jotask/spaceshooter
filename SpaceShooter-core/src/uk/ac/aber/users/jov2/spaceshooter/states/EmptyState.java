package uk.ac.aber.users.jov2.spaceshooter.states;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import uk.ac.aber.users.jov2.spaceshooter.util.GameStateManager;
import uk.ac.aber.users.jov2.spaceshooter.util.GameStateManager.StateHolder;

public class EmptyState implements State {

	@Override
	public void init() { }

	@Override
	public void update(GameStateManager gsm, float delta) {
		gsm.changeState(StateHolder.SPLASH, true);
	}

	@Override
	public void render(SpriteBatch sb) { }

	@Override
	public void debug(ShapeRenderer sr) { }

	@Override
	public void dispose() { }

}
