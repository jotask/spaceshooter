package uk.ac.aber.users.jov2.spaceshooter.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import uk.ac.aber.users.jov2.spaceshooter.game.Game;
import uk.ac.aber.users.jov2.spaceshooter.game.ui.Pause;
import uk.ac.aber.users.jov2.spaceshooter.util.Constants;
import uk.ac.aber.users.jov2.spaceshooter.util.GameStateManager;

public class GameState implements State {

	public enum StateGame {PAUSE, RUN}
	private StateGame currentState;
	public static InputProcessor inputProcessor;
	
	private Skin skin;
	
	private Pause pause;
	private Game game;
	
	@Override
	public void init() {
		skin = new Skin(Gdx.files.internal("skin/uiskin.json"));
		pause = new Pause(this, skin);
		game = new Game();
		game.init();
		
		currentState = StateGame.RUN;
		inputProcessor = Gdx.input.getInputProcessor();
		Gdx.gl.glClearColor(.135f, .206f, .235f, 1);
	}

	@Override
	public void update(GameStateManager gsm, float delta) {
		handleInput();
		switch (currentState){
		case PAUSE:
			pause.update(delta);
			break;
		case RUN:
			game.update(gsm, delta);
			break;
		}
	}
	
	private void handleInput(){
		switch (currentState){
			case PAUSE:
				if(Gdx.input.isKeyJustPressed(Constants.PAUSE)){
					currentState = StateGame.RUN;
					pause.enter();
				}
				break;
			case RUN:
				if(Gdx.input.isKeyJustPressed(Constants.PAUSE)){
					currentState = StateGame.PAUSE;
					pause.enter();
				}
				break;
		}
	}

	@Override
	public void render(SpriteBatch sb) {
		Gdx.gl.glClearColor(.135f, .206f, .235f, 1);
		game.render(sb);
		if(currentState == StateGame.PAUSE)
			pause.render();
	}

	@Override
	public void debug(ShapeRenderer sr) {
		game.debug(sr);
	}

	@Override
	public void dispose() {
		
	}
	
	public void setState(StateGame sg){
		currentState = sg;
	}

}
