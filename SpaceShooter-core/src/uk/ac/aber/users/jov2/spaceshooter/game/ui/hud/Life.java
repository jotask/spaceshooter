package uk.ac.aber.users.jov2.spaceshooter.game.ui.hud;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import uk.ac.aber.users.jov2.spaceshooter.game.entity.Player;

public class Life extends Actor {

	private Player player;
	private Texture hearth;
	private Table table;

	public Life(Table table, Player player) {
		this.table = table;
		this.player = player;
		this.hearth = new Texture(Gdx.files.internal("ui/hearth.png"));
	}

	@Override
	public void act(float delta) {
		super.act(Math.min(delta, 1 / 30f));
	}

	private void renderHearth(Batch sb) {
		float life = player.getLife();
		float x = getX();
		for (float i = 0; i < player.getLife(); i += 10) {
			if (life % player.HEALTH == 0) {
				sb.draw(hearth, x, getY());
				x += hearth.getWidth();
			}
		}

	}

	@Override
	public void draw(Batch sb, float parentAlpha) {
		super.draw(sb, parentAlpha);
		this.renderHearth(sb);
	}

	public void dispose() {
		hearth.dispose();
	}

}
