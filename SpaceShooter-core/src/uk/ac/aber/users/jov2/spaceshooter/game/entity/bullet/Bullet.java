package uk.ac.aber.users.jov2.spaceshooter.game.entity.bullet;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;

import uk.ac.aber.users.jov2.spaceshooter.game.entity.Entity;
import uk.ac.aber.users.jov2.spaceshooter.util.Constants;
import uk.ac.aber.users.jov2.spaceshooter.util.effects.BulletFx;

public class Bullet extends Entity{
	
	private final float SPEED;
	private Texture texture;
	
	private BulletFx fx;
	
	private boolean delete;
	
	public static final float SIZE = 2f;
	private float magnitude;
	
	public final float DAMAGE = 10f;
	
	private static float DEATH = 3f;
	private float currentTime;
	
	private boolean isPlayer;
	
	public Bullet(Vector2 v, boolean isPlayer) {
		this(v, 5000, 0, isPlayer);
	}
	
	public Bullet(Vector2 v, float speed, float magnitude, boolean isPlayer) {
		this.SPEED = speed;
		this.magnitude = magnitude;
		this.isPlayer = isPlayer;
		texture = new Texture(Gdx.files.internal("ufoRed.png"));
		init(v);
	}
	
	private  void init(Vector2 v){
		BodyDef bd = new BodyDef();
		bd.bullet = true;
		bd.fixedRotation = true;
		bd.position.set(v);
		bd.type = BodyType.DynamicBody;
		bd.fixedRotation = true;
		
		CircleShape circle = new CircleShape();
		circle.setRadius(SIZE);
		
		FixtureDef fd = new FixtureDef();
		fd.shape = circle;
		if(!isPlayer){
			fd.filter.categoryBits = Constants.ENEMY_BULLET_BITS;
			fd.filter.maskBits = Constants.ENEMY_BULLET_MASK;
		}else{
			fd.filter.categoryBits = Constants.PLAYER_BULLET_BITS;
			fd.filter.maskBits = Constants.PLAYER_BULLET_MASK;
		}
		
		this.createBody(bd, circle, fd);		
		body.setBullet(true);
		body.setUserData(this);
		body.setTransform(body.getPosition(), magnitude);
		
		fx = new BulletFx();
		fx.setScale(0.2f);
		
		if(isPlayer){
			body.setLinearVelocity(SPEED, 0);
		}else{
			body.setLinearVelocity(-SPEED, 0);
			float angle = 180;
			fx.getEffect().getEmitters().first().getAngle().setLow(angle);
			fx.getEffect().getEmitters().first().getAngle().setHigh(angle);
		}
		
	}
	
	public void update(float delta){
		currentTime += delta;
		fx.setsetEmitorPosition(body.getPosition());
		fx.update(delta);
	}
	
	public void render(SpriteBatch sb){
		Vector2 p = body.getPosition();
		sb.draw(texture, p.x - SIZE, p.y - SIZE, SIZE * 2, SIZE * 2);
		fx.render(sb);
	}
	
	public void setDeath(boolean d){
		this.delete = d;
	}
	
	public boolean death(){
		if(delete || currentTime >= DEATH){
			return true;
		}
		return false;
	}
	
	public boolean delete(){
		return this.delete;
	}
	
	public float getDamage(){
		return this.DAMAGE;
	}

	@Override
	public void debug(ShapeRenderer sr) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}
