package uk.ac.aber.users.jov2.spaceshooter.util;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.alpha;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.parallel;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class Banner extends Actor {

	private Texture texture;

	public Banner() {

		texture = new Texture(Gdx.files.internal("logo_2.png"));

		this.setSize(texture.getWidth(), texture.getHeight());

		this.addAction(sequence(alpha(0), parallel(fadeIn(.5f))));

	}

	@Override
	public void draw(Batch sb, float parentAlpha) {
		super.draw(sb, parentAlpha);
		sb.draw(texture, this.getX(), getY(), this.getOriginX(), this.getOriginY(), this.getWidth(), this.getHeight(),
				this.getScaleX(), this.getScaleY(), this.getRotation(), 0, 0, texture.getWidth(), texture.getHeight(),
				false, false);
	}
	
	public void dispose(){
		texture.dispose();
	}

}
