package uk.ac.aber.users.jov2.spaceshooter.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

import uk.ac.aber.users.jov2.spaceshooter.Application;
import uk.ac.aber.users.jov2.spaceshooter.util.GameStateManager;
import uk.ac.aber.users.jov2.spaceshooter.util.GameStateManager.StateHolder;

public class Options implements State {

	private Skin skin;
	private Stage stage;
	private Table table;
	
	@Override
	public void init() {
		skin = new Skin(Gdx.files.internal("skin/uiskin.json"));
		stage = new Stage();
		table = new Table();
		table.setFillParent(true);
		stage.addActor(table);
		
		addButtons();
		
		Gdx.input.setInputProcessor(stage);
		Gdx.gl.glClearColor(.135f, .206f, .235f, 1);
	}
	
	private void addButtons(){
		final TextButton back = new TextButton("Back", skin);
		back.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				Application.gsm.changeState(StateHolder.MENU);
			}
		});
		table.add(back);
	}

	@Override
	public void update(GameStateManager gsm, float delta) {
		stage.act(delta);
	}

	@Override
	public void render(SpriteBatch sb) {
		table.setDebug(Application.debug);
		stage.draw();
	}

	@Override
	public void debug(ShapeRenderer sr) {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
