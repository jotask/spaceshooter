package uk.acer.aber.users.jov2.spaceshooter.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import uk.ac.aber.users.jov2.spaceshooter.Application;

public class DesktopLauncher {
	
	public static void main (String[] arg) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.width = 800;
		cfg.height = 600;
		cfg.fullscreen = false;
		cfg.resizable = false;
		new LwjglApplication(new Application(), cfg);
	}
	
}
