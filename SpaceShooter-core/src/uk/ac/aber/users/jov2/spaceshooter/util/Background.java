package uk.ac.aber.users.jov2.spaceshooter.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class Background {
	
	private Texture texture;
	private Vector2 position;
	private Vector2 size;
	
	public Background(String image, Camera camera) {
		this(image, camera, TextureFilter.Nearest);
	}
	
	public Background(String image, Camera cam, TextureFilter filter) {
		texture = new Texture(Gdx.files.internal(image));
		texture.setFilter(filter, filter);
		
		position = new Vector2(cam.position.x - (cam.viewportWidth / 2), cam.position.y - (cam.viewportHeight / 2));
		size = new Vector2(cam.viewportWidth, cam.viewportHeight);
		
	}
	
	public void render(SpriteBatch sb){
		sb.draw(texture, position.x, position.y, size.x, size.y);
	}
	
	public void dispose(){
		texture.dispose();
	}

}
