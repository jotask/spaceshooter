package uk.ac.aber.users.jov2.spaceshooter.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;

import uk.ac.aber.users.jov2.spaceshooter.util.Camera;
import uk.ac.aber.users.jov2.spaceshooter.util.GameStateManager;
import uk.ac.aber.users.jov2.spaceshooter.util.GameStateManager.StateHolder;

public class SplashScreen implements State {

	private Camera cam;
	private static final long timetoWait = 3l;
	private long time;
	
	private Texture aiko;
	private Vector2 position;
	
	@Override
	public void init() {
		cam = new Camera();
		aiko = new Texture(Gdx.files.internal("aiko.png"));
		position = new Vector2(cam.position.x - (aiko.getWidth() / 2), cam.position.y - (aiko.getHeight() / 2));
		time = System.currentTimeMillis() + (timetoWait * 1000);
	}

	@Override
	public void update(GameStateManager gsm, float delta) {
		if(System.currentTimeMillis() > time)
			gsm.changeState(StateHolder.PRESSSTART, true);
	}

	@Override
	public void render(SpriteBatch sb) {
		sb.setProjectionMatrix(cam.combined);
		sb.begin();
		sb.draw(aiko, position.x, position.y);
		sb.end();
	}

	@Override
	public void debug(ShapeRenderer sr) {
		sr.setProjectionMatrix(cam.combined);
		sr.begin(ShapeType.Line);
		sr.setColor(Color.RED);
		sr.box(position.x, position.y, 0, aiko.getWidth(), aiko.getHeight(), 0);
		sr.end();
	}

	@Override
	public void dispose() {
		aiko.dispose();
	}

}
