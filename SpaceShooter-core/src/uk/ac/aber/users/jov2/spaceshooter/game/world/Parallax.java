package uk.ac.aber.users.jov2.spaceshooter.game.world;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import uk.ac.aber.users.jov2.spaceshooter.util.Camera;

public class Parallax {
	
	private Texture texture;
	private Vector2 p1, p2;
	private Camera cam;
	
	public Parallax(Camera cam) {
		this.cam = cam;
		texture = new Texture(Gdx.files.internal("bg.png"));
		
		Vector2 init = cam.get2D();
		
		p1 = new Vector2(init.x - (cam.viewportWidth / 2), init.y - (cam.viewportHeight / 2));
		p2 = new Vector2(p1.x + cam.viewportWidth, p1.y);
		
	}
	
	public void update(float delta){
		
	}
	
	public void render(SpriteBatch sb){
		sb.draw(texture, p1.x, p1.y, cam.viewportWidth, cam.viewportHeight);
		sb.draw(texture, p2.x, p2.y, cam.viewportWidth, cam.viewportHeight);
	}
	
	public void dipose(){
		texture.dispose();
	}

}
