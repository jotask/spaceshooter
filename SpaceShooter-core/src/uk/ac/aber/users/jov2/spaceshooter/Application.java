package uk.ac.aber.users.jov2.spaceshooter;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import uk.ac.aber.users.jov2.spaceshooter.util.Constants;
import uk.ac.aber.users.jov2.spaceshooter.util.GameStateManager;
import uk.ac.aber.users.jov2.spaceshooter.util.Util;

public class Application implements ApplicationListener {
	
	SpriteBatch sb;
	ShapeRenderer sr;
	public static GameStateManager gsm;
	public static boolean debug = true;

	@Override
	public void create() {
		sb = new SpriteBatch();
		sr = new ShapeRenderer();
		gsm = new GameStateManager();
		
		Util.changeBGColor(1, 1, 1, 1);
		
	}

	@Override
	public void resize(int width, int height) { }

	@Override
	public void render() {
		// Update methods
		if(Gdx.input.isKeyJustPressed(Constants.DEBUG))
			toggleDebug();
		gsm.update(Gdx.graphics.getDeltaTime());
		
		// Render Methods
		Gdx.graphics.getGL20().glClear( GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT );
		gsm.render(sb);
		if(debug)
			gsm.debug(sr);
	}
	
	private void toggleDebug(){
		debug = !debug;
	}

	@Override
	public void pause() { }

	@Override
	public void resume() { }

	@Override
	public void dispose() {
		gsm.dispose();
		sb.dispose();
		sr.dispose();
	}
	
}
