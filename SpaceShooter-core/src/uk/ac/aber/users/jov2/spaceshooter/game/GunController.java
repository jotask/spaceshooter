package uk.ac.aber.users.jov2.spaceshooter.game;

import uk.ac.aber.users.jov2.spaceshooter.game.entity.Entity;
import uk.ac.aber.users.jov2.spaceshooter.game.entity.Player;

public class GunController {
	
	private Gun gun;
	private Entity e;
	
	public GunController(Entity e) {
		this.e = e;
		boolean isPlayer = false;
		if(e instanceof Player)
			isPlayer = true;
		gun = new Gun(isPlayer);
	}
	
	public void shoot(){
		if(gun != null)
			gun.shoot(e.getPosition());
	}

}
