package uk.ac.aber.users.jov2.spaceshooter.game.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

import uk.ac.aber.users.jov2.spaceshooter.Application;
import uk.ac.aber.users.jov2.spaceshooter.states.GameState;
import uk.ac.aber.users.jov2.spaceshooter.states.GameState.StateGame;
import uk.ac.aber.users.jov2.spaceshooter.util.GameStateManager.StateHolder;

public class Pause implements UI{

	private Stage stage;
	private Table table;
	private Skin skin;
	private final GameState game;
	
	
	public Pause(GameState game, Skin skin) {
		this.game = game;
		this.skin = skin;
		
		stage = new Stage();
		
		table = new Table();
		table.setFillParent(true);
		table.setDebug(true);
		stage.addActor(table);
		
		addActor();
		
	}
	
	private void addActor(){
		
		final TextButton gameButton = new TextButton("Keep Playing", skin);
		gameButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				Gdx.input.setInputProcessor(GameState.inputProcessor);
				game.setState(StateGame.RUN);
			}
		});
		table.add(gameButton);
		table.row();
		
		final TextButton back = new TextButton("Back to Menu", skin);
		back.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				Application.gsm.changeState(StateHolder.MENU);
			}
		});
		table.add(back);
		
	}
	
	@Override
	public void enter(){
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void update(float delta) {
		table.setDebug(Application.debug);
		stage.act(delta);
	}

	@Override
	public void render() {
		stage.draw();
	}
	
}
