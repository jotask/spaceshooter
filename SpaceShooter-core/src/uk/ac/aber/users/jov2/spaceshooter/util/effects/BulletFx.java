package uk.ac.aber.users.jov2.spaceshooter.util.effects;

import com.badlogic.gdx.graphics.g2d.ParticleEffect;

public class BulletFx extends Particle{
	
	public BulletFx() {
		super("particles/bulletFx");
	}

	public ParticleEffect getEffect(){
		return this.particle;
	}
	
}
