package uk.ac.aber.users.jov2.spaceshooter.game.ui;

public interface UI {
	
	public void enter();
	
	public void update(float delta);
	
	public void render();

}
