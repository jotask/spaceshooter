package uk.ac.aber.users.jov2.spaceshooter.util;

import java.util.HashMap;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import uk.ac.aber.users.jov2.spaceshooter.states.EmptyState;
import uk.ac.aber.users.jov2.spaceshooter.states.GameState;
import uk.ac.aber.users.jov2.spaceshooter.states.Menu;
import uk.ac.aber.users.jov2.spaceshooter.states.Options;
import uk.ac.aber.users.jov2.spaceshooter.states.PressStart;
import uk.ac.aber.users.jov2.spaceshooter.states.SplashScreen;
import uk.ac.aber.users.jov2.spaceshooter.states.State;

public class GameStateManager{
	
	public enum StateHolder{ EMPTY, SPLASH, PRESSSTART, MENU, OPTIONS, GAME; }
	private HashMap<StateHolder, State> states;
	StateHolder currentState;
	
	public GameStateManager(){
		states = new HashMap<StateHolder, State>();
		initStates();
		
		currentState = StateHolder.GAME;
		states.get(currentState).init();
	}
	
	private void initStates(){
		states.put(StateHolder.EMPTY, new EmptyState());
		states.put(StateHolder.SPLASH, new SplashScreen());
		states.put(StateHolder.PRESSSTART, new PressStart());
		states.put(StateHolder.MENU, new Menu());
		states.put(StateHolder.OPTIONS, new Options());
		states.put(StateHolder.GAME, new GameState());
	}
	
	public void changeState(StateHolder state){
		changeState(state, false);
	}
	
	public void changeState(StateHolder state, boolean remove){
		states.get(currentState).dispose();
		if(remove)
			states.remove(currentState);
		currentState = state;
		states.get(currentState).init();
	}
	
	public void update(float delta){
		states.get(currentState).update(this, delta);
	}
	
	public void render(SpriteBatch sb){
		states.get(currentState).render(sb);
	}
	
	public void debug(ShapeRenderer sr){
		states.get(currentState).debug(sr);
	}
	
	public void dispose(){
		states = null;
	}

}
