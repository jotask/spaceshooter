package uk.ac.aber.users.jov2.spaceshooter.game.entity;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Shape;

import uk.ac.aber.users.jov2.spaceshooter.game.Game;

public abstract class Entity{
	
	protected Body body;
	protected Fixture fixture;
	
	protected void createBody(BodyDef bd, Shape shape, FixtureDef fd){
		// Create our body in the world using our body definition
		body = Game.level.getWorld().createBody(bd);
		// Create our fixture and attach it to the body
		fixture = body.createFixture(fd);
		shape.dispose();
	}

	public abstract void update(float delta);
	public abstract void render(SpriteBatch sb);
	public abstract void debug(ShapeRenderer sr);
	public abstract void dispose();
	
	public Vector2 getPosition(){
		return body.getPosition();
	}
	
	public Body getBody(){
		return this.body;
	}

}
