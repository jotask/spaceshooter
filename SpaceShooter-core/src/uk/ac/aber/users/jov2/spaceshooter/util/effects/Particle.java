package uk.ac.aber.users.jov2.spaceshooter.util.effects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public abstract class Particle {
	
	protected ParticleEffect particle;
	
	protected Particle(String file) {
		particle = new ParticleEffect();
		particle.load(Gdx.files.internal(file), Gdx.files.internal(""));
		particle.getEmitters().first().setPosition(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2);
//		particle.scaleEffect(2);
		particle.start();
	}
	
	public void setScale(float scale){
		particle.scaleEffect(scale);
	}
	
	public void update(float delta){
		particle.update(delta);
	}
	
	public void setsetEmitorPosition(Vector2 position){
		this.setEmitorPosition(position.x, position.y);
	}
	
	public void setEmitorPosition(float x, float y){
		particle.getEmitters().first().setPosition(x, y);
	}
	
	public void render (SpriteBatch sb){
		particle.draw(sb);
	}
	
	public void reset(){
		if(particle.isComplete())
			particle.reset();
	}
	
	public void dispose(){
		particle.dispose();
	}

}
