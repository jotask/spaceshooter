package uk.ac.aber.users.jov2.spaceshooter.game.world;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;

import uk.ac.aber.users.jov2.spaceshooter.util.Camera;

public class Level{

	private final World world;
	private Box2DDebugRenderer debugRenderer;
	private Bound bound;
	private Parallax parallax;
	
	public Level(Camera cam) {
		world = new World(new Vector2(0, 0), true);
		world.setContactListener(new Collisions(world));
		debugRenderer = new Box2DDebugRenderer();
		bound = new Bound(world, cam);
		parallax = new Parallax(cam);
	}
	
	public void update(float delta, Camera cam){
		world.step(1/60f, 6, 2);
		bound.update(cam.get2D());
		parallax.update(delta);
	}
	
	public void render(SpriteBatch sb){
		parallax.render(sb);
	}
	
	public void debug(Matrix4 matrix){
		debugRenderer.render(world, matrix);
	}
	
	public void dispose(){
		parallax.dipose();
		world.dispose();
		debugRenderer.dispose();
	}
	
	public World getWorld(){ return this.world; }
	
}
