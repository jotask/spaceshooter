package uk.ac.aber.users.jov2.spaceshooter.util;

import com.badlogic.gdx.Input.Keys;

public class Constants {
	
	// Input Keys
	public static final int DEBUG = Keys.TAB;
	public static final int PAUSE = Keys.ESCAPE;
	
	public static final int RIGTH = Keys.D;
	public static final int LEFT = Keys.A;
	public static final int UP = Keys.W;
	public static final int DOWN = Keys.S;

	public static final int A = Keys.E;
	public static final int B = Keys.Q;
	
	// Collision filters
	public static final short BOUNDS_BITS = 2;
	public static final short PLAYER_BITS = 4;
	public static final short ENEMY_BITS = 8;
	public static final short PLAYER_BULLET_BITS = 16;
	public static final short ENEMY_BULLET_BITS = 32;
	
	public static final short BOUNDS_MASK = -1;
	public static final short PLAYER_MASK = BOUNDS_BITS | ENEMY_BITS | ENEMY_BULLET_BITS;
	public static final short ENEMY_MASK = BOUNDS_BITS | PLAYER_BITS | PLAYER_BULLET_BITS;
	public static final short PLAYER_BULLET_MASK = BOUNDS_BITS | ENEMY_BITS | ENEMY_BULLET_BITS;
	public static final short ENEMY_BULLET_MASK = BOUNDS_BITS | PLAYER_BITS | PLAYER_BULLET_BITS;

}
