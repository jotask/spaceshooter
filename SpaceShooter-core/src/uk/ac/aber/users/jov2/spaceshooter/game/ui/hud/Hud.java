package uk.ac.aber.users.jov2.spaceshooter.game.ui.hud;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import uk.ac.aber.users.jov2.spaceshooter.Application;
import uk.ac.aber.users.jov2.spaceshooter.game.entity.Player;
import uk.ac.aber.users.jov2.spaceshooter.game.ui.UI;

public class Hud implements UI{
	
	private Stage stage;
	private Table table;
	
	public Hud(Player player) {
		stage = new Stage();
		table = new Table();
		stage.addActor(table);
		Life life = new Life(table, player);
		table.add(life).fill().center();
	}

	@Override
	public void update(float delta) {
		table.setDebug(Application.debug);
		stage.act(delta);
	}

	@Override
	public void render() {
		stage.draw();
	}

	@Override
	public void enter() {
		stage.dispose();
	}

}
