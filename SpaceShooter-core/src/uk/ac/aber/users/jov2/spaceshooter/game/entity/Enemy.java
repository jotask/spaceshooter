package uk.ac.aber.users.jov2.spaceshooter.game.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;

import uk.ac.aber.users.jov2.spaceshooter.game.GunController;
import uk.ac.aber.users.jov2.spaceshooter.util.Constants;
import uk.ac.aber.users.jov2.spaceshooter.util.Sprite;

public class Enemy extends LiveEntity{
	
	private static final float SIZE = 6;

	private static final float HEALTH = 100;
	
	private GunController gun;
	
	private Sprite sprite;
	
	public Enemy() {
		super(HEALTH);
		// First we create a body definition
		BodyDef bodyDef = new BodyDef();
		// We set our body to dynamic, for something like ground which doesn't move we would set it to StaticBody
		bodyDef.type = BodyType.DynamicBody;
		// Set our body's starting position in the world
		bodyDef.position.set(75, 0);

		// Create a circle shape and set its radius to 6
		CircleShape circle = new CircleShape();
		circle.setRadius(SIZE);

		// Create a fixture definition to apply our shape to
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = circle;
		fixtureDef.density = 0.01f; 
		fixtureDef.friction = 100f;
		fixtureDef.filter.categoryBits = Constants.ENEMY_BITS;
		fixtureDef.filter.maskBits = Constants.ENEMY_MASK;
		
		this.createBody(bodyDef, circle, fixtureDef);
		body.setFixedRotation(true);
		body.setUserData(this);
		
		sprite = new Sprite(Gdx.files.internal("enemyRed1.png"), SIZE, SIZE);
		
		currentHealth = health;
		
		gun = new GunController(this);
	}
	
	private void enemyShoot(){
		float rate = 10f;
		float random = MathUtils.random(100f);
		if(rate > random){
			gun.shoot();
//			System.out.println("enemy shooting " + random);
		}
	}

	@Override
	public void update(float delta) {
		enemyShoot();
	}

	@Override
	public void render(SpriteBatch sb) {
		sprite.render(sb, body.getPosition());
	}

	@Override
	public void debug(ShapeRenderer sr) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		sprite.dispose();
	}
	

	@Override
	public Vector2 getPosition() {
		return body.getPosition();
	}
	
	public Body getBody(){
		return this.body;
	}

}
