package uk.ac.aber.users.jov2.spaceshooter.util.input;

import uk.ac.aber.users.jov2.spaceshooter.util.Constants;

public class KonamiCode {
	
	private static final int CODE[] = {
			Constants.UP,
			Constants.UP,
			Constants.DOWN,
			Constants.DOWN,
			Constants.LEFT,
			Constants.RIGTH,
			Constants.LEFT,
			Constants.RIGTH,
			Constants.B,
			Constants.A
	};
	
	private int current = 0;
	
	public void keyPressed(int keycode){
		if(CODE[current] == keycode){
//			System.out.println("correct");
			if(current == CODE.length - 1){
				System.out.println("KONAMI CODE COMPLETED");
			}else{
				current++;
			}
		}else{
			current = 0;
//			System.out.println("reset Konami Code");
		}
	}

}
