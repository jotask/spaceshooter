package uk.ac.aber.users.jov2.spaceshooter.states;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import uk.ac.aber.users.jov2.spaceshooter.util.GameStateManager;

public interface State {
	
	public abstract void init();
	public abstract void update(GameStateManager gsm, float delta);
	public abstract void render(SpriteBatch sb);
	
	public void debug(ShapeRenderer sr);
	public void dispose();
	
}
