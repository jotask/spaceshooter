package uk.ac.aber.users.jov2.spaceshooter.util;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class Sprite{
	
	private float width, height;
	private Texture texture;

	public Sprite(FileHandle file, float width, float height) {
		this(new Texture(file), width, height);
	}
	public Sprite(Texture texture, float width, float height){
		this.texture = texture;
		this.width = width;
		this.height = height;
	}
	
	public void render(SpriteBatch sb, Vector2 position){
		sb.draw(texture, position.x - width, position.y - height, width * 2, height * 2);
	}
	
	public Texture getTexture(){
		return texture;
	}
	
	public void dispose(){
		texture.dispose();
	}

}
