package uk.ac.aber.users.jov2.spaceshooter.game.world;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;

import uk.ac.aber.users.jov2.spaceshooter.game.entity.Enemy;
import uk.ac.aber.users.jov2.spaceshooter.game.entity.Player;
import uk.ac.aber.users.jov2.spaceshooter.game.entity.bullet.Bullet;

public class Collisions implements ContactListener {
		
	public Collisions(World world) {
	}

	@Override
	public void beginContact(Contact contact) {
		Object a = contact.getFixtureA().getBody().getUserData();
		Object b = contact.getFixtureB().getBody().getUserData();

		if(bulletEnemy(a, b)){
			Enemy ent = null;
			Bullet bul = null;
			if(a instanceof Enemy){
				ent = (Enemy) a;
				bul = (Bullet) b;
			}else{
				ent = (Enemy) b;
				bul = (Bullet) a;
			}
			this.enemyHit(ent, bul);
		}else if(bulletBound(a, b)){
			Bullet bullet;
			if(a instanceof Bullet){
				bullet = (Bullet) a;
			}else{
				bullet = (Bullet) b;
			}
			bullet.setDeath(true);
		}else if(bulletPlayer(a, b)){
			Bullet bullet = null;
			Player player = null;
			if(a instanceof Bullet){
				bullet = (Bullet) a;
				player = (Player) b;
			}else{
				bullet = (Bullet) b;
				player = (Player) a;
			}
			player.takeDamage(bullet.DAMAGE);
			bullet.setDeath(true);
		}else if(bulletBullet(a, b)){
			Bullet bulletA = (Bullet) a;
			Bullet bulletB = (Bullet) b;
			bulletA.setDeath(true);
			bulletB.setDeath(true);
		}
		
	}
	
	private boolean bulletBullet(Object a, Object b){
		if(a instanceof Bullet && b instanceof Bullet)
			return true;
		return false;
	}
	
	private boolean bulletEnemy(Object a, Object b){
		if((a instanceof Enemy && b instanceof Bullet) || (a instanceof Bullet && b instanceof Enemy)){
			return true;
		}
		return false;
	}
	
	private boolean bulletPlayer(Object a, Object b){
		if((a instanceof Player && b instanceof Bullet) || (a instanceof Bullet && b instanceof Player)){
			return true;
		}
		return false;
	}
	
	private boolean bulletBound(Object a, Object b){
		if((a instanceof Bound && b instanceof Bullet) || (a instanceof Bullet && b instanceof Bound)){
			return true;
		}
		return false;
	}

	@Override
	public void endContact(Contact contact) {
		
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		
	}
	
	private void enemyHit(Enemy a, Bullet b){
		Enemy enemy = (Enemy) a;
		Bullet bullet = (Bullet) b;
		enemy.takeDamage(bullet.getDamage());
		bullet.setDeath(true);
		
	}

}
