package uk.ac.aber.users.jov2.spaceshooter.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import uk.ac.aber.users.jov2.spaceshooter.Application;
import uk.ac.aber.users.jov2.spaceshooter.util.Background;
import uk.ac.aber.users.jov2.spaceshooter.util.Banner;
import uk.ac.aber.users.jov2.spaceshooter.util.GameStateManager;
import uk.ac.aber.users.jov2.spaceshooter.util.GameStateManager.StateHolder;
import uk.ac.aber.users.jov2.spaceshooter.util.effects.Stars;
import uk.ac.aber.users.jov2.spaceshooter.util.input.KonamiInputProcessor;

public class PressStart implements State {

	private Stage stage;
	private Table table;
	private Skin skin;
	
	private Banner logo;
	private Background bg;
	private Stars particle;
	
	@Override
	public void init() {
		skin = new Skin(Gdx.files.internal("skin/uiskin.json"));
		
		stage = new Stage();
		table = new Table();
		table.setFillParent(true);
		stage.addActor(table);
		
		logo = new Banner();
		bg = new Background("bg.png", stage.getCamera());
		particle = new Stars();
		
		table.add(logo).row();
		
		addActors();
		
		InputProcessor konami = new KonamiInputProcessor();
		InputProcessor processor = stage;
		InputMultiplexer multiplexer = new InputMultiplexer();
		multiplexer.addProcessor(konami);
		multiplexer.addProcessor(processor);
		
		Gdx.input.setInputProcessor(multiplexer);
		Gdx.gl.glClearColor(.135f, .206f, .235f, 1);
	}
	
	private void addActors(){
		final TextButton pressStart = new TextButton("Press Start", skin);
		pressStart.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				Application.gsm.changeState(StateHolder.MENU);
			}
		});
		table.add(pressStart).fill();
		
	}

	@Override
	public void update(GameStateManager gsm, float delta) {
		particle.update(delta);
		stage.setDebugAll(Application.debug);
		stage.act(delta);
	}

	@Override
	public void render(SpriteBatch sb) {
		sb.setProjectionMatrix(stage.getCamera().combined);
		sb.begin();
		bg.render(sb);
		particle.render(sb);
		sb.end();
		
		stage.draw();
		particle.reset();
	}

	@Override
	public void debug(ShapeRenderer sr) { }

	@Override
	public void dispose() {
		skin.dispose();
		stage.dispose();
		logo.dispose();
		particle.dispose();
	}

}
