package uk.ac.aber.users.jov2.spaceshooter.game;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.TimeUtils;

import uk.ac.aber.users.jov2.spaceshooter.game.entity.bullet.Bullet;

public class Gun {
	
	private final long timeBetweenShoots;
	private long nextShoot;
	
	private boolean isPlayer;
	
	public Gun(boolean isPlayer){
		this(100l, isPlayer); // Default one second between shoots
	}
	
	public Gun(long between, boolean isPlayer) {
		this.timeBetweenShoots = between;
		this.isPlayer = isPlayer;
		
		nextShoot();
	}
	
	public void shoot(Vector2 v){
		if(nextShoot < TimeUtils.millis()){
			Bullet b = new Bullet(v, isPlayer);
			nextShoot();
			createShoot(b);
		}
	}
	
	private void createShoot(Bullet b){
		Game.bullets.add(b);
	}
	
	private void nextShoot(){
		nextShoot = TimeUtils.millis() + timeBetweenShoots;
	}

}
